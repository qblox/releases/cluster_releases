# Cluster releases

Copyright (C) Qblox BV (2022) - All rights reserved

This repository contains the firmware releases for the Qblox Cluster. You can download the releases from
the [Gitlab release page](https://gitlab.com/qblox/releases/cluster_releases/-/releases). Follow the instructions below
to update your Cluster with the latest firmware release.

## Prerequisites:
- [Python 3.8](https://www.python.org/downloads/release/python-380/)
- [Qblox Instruments](https://pypi.org/project/qblox-instruments/)

## Install Qblox Instruments

- Open a terminal.
- Execute: `pip install qblox-instruments`

For more information, visit the [setup page](https://qblox-qblox-instruments.readthedocs-hosted.com/en/main/getting_started/setup.html)
on the Qblox Instruments Read The Docs pages.

## Update the firmware

- Download and extract the [latest release](https://gitlab.com/qblox/releases/cluster_releases/-/releases/permalink/latest#release).
- In a terminal, go to the location of the extracted files.
- Execute: `qblox-cfg <ip-address> update update.tar.gz`<sup>1</sup>
- Follow the instructions on the screen.<sup>2</sup>

For more information, visit the [updating page](https://qblox-qblox-instruments.readthedocs-hosted.com/en/master/getting_started/updating.html)
on the Qblox Instruments Read The Docs pages.

---
**NOTES**

1: If your Cluster / modules are running version v0.1.x, this update procedure needs to be executed for every module. It is imperative that the CMM is updated last. For all updates onward, the Cluster is updated as a single instrument.

2: If the device has not rebooted after a minute, please remove power from the device for a minute before powering it on again. This forces the device to do a hard reboot. This has to be repeated twice with a one minute interval to complete the update. The update is complete when the status LED turns green again.

---
